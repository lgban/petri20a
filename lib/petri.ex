defmodule Petri do
  def ejemplo1 do
    {
      ~w[A B C D E],
      ~w[p0 p1 p2 p3 p4 p5],
      [
        ~w[p0 A], ~w[A p1], ~w[A p2], ~w[p1 B], ~w[p1 D], ~w[p2 D], ~w[p2 C],
        ~w[B p3], ~w[D p3], ~w[D p4], ~w[C p4], ~w[p3 E], ~w[p4 E], ~w[E p5]
      ]
    }
  end

  def ejemplo2 do
    {
      ~w[A B C D E],
      ~w[p0 p1 p2 p3 p4 p5],
      [
        ~w[p0 A], ~w[A p1], ~w[A p2], ~w[p1 B], ~w[p4 D], ~w[D p2], ~w[p2 C],
        ~w[B p3], ~w[C p4], ~w[p3 E], ~w[p4 E], ~w[E p5]
      ]
    }
  end

  def ejemplo3 do
    {
      ~w[A B C D E],
      ~w[p0 p1 p2 p3 p4 p5],
      [
        ~w[p0 A], ~w[A p1], ~w[A p2], ~w[p1 B], ~w[p1 D], ~w[p2 C],
        ~w[B p3], ~w[D p3], ~w[C p4], ~w[p3 E], ~w[p4 E], ~w[E p5]
      ]
    }
  end

  def preset(f, t) do
    Enum.filter(f, fn [_, e] -> e == t end)
    |> Enum.map(fn [e, _] -> e end)
    |> MapSet.new
  end
  
  def postset(f, t) do
    Enum.filter(f, fn [e, _] -> e == t end)
    |> Enum.map(fn [_, e] -> e end)
    |> MapSet.new
  end

  def is_enabled?(l, m, t) do
    MapSet.subset?(preset(l, t), m)
  end

  def fire(l, m, t) do
    if is_enabled?(l, m, t) do
      # (m \ *t) U t*
      MapSet.difference(m, preset(l, t)) 
      |> MapSet.union(postset(l, t))
    else
      m
    end
  end
  def enablement(l, m, ts) do
    Enum.filter(ts, fn t -> is_enabled?(l, m, t) end)
  end
end
